package net.luminis.dm.demo.dashboard.probe.bundles;

import net.luminis.dm.demo.dashboard.Probe;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		Component servicesProbe = manager.createComponent()
				.setImplementation(BundlesProbe.class)
				.setInterface(Probe.class.getName(), null);
		manager.add(servicesProbe);
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {

	}

}
