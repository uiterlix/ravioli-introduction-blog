package net.luminis.dm.demo.dashboard.probe.services;

import net.luminis.dm.demo.dashboard.Probe;

import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;

public class ServicesProbe implements Probe {
	
	private BundleContext context;

	@Override
	public String getName() {
		return "# services";
	}

	@Override
	public String getValue() {
		try {
			return String.valueOf(context.getAllServiceReferences(null, "(objectClass=*)").length);
		} catch (InvalidSyntaxException e) {
			// We're pretty sure our filter syntax is correct.
		}
		return null;
	}
	
	void init(Component component) {
		context = component.getDependencyManager().getBundleContext();
	}

}
