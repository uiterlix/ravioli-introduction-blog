package net.luminis.dm.demo.dashboard.probe.bundles;
import net.luminis.dm.demo.dashboard.Probe;

import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;


public class BundlesProbe implements Probe {

	private BundleContext context;

	@Override
	public String getName() {
		return "# bundles";
	}

	@Override
	public String getValue() {
		return String.valueOf(context.getBundles().length);
	}
	
	void init(Component component) {
		context = component.getDependencyManager().getBundleContext();
	}
}
