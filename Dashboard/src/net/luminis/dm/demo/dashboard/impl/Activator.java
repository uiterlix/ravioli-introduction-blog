package net.luminis.dm.demo.dashboard.impl;

import net.luminis.dm.demo.dashboard.Probe;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		Component dashboard = manager.createComponent()
				.setImplementation(Dashboard.class);
		dashboard.add(manager.createServiceDependency()
				.setService(Probe.class)
				.setCallbacks("addProbe", "removeProbe")
				.setRequired(true));
		manager.add(dashboard);
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
	}

}
