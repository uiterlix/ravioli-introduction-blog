package net.luminis.dm.demo.dashboard.impl;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.luminis.dm.demo.dashboard.Probe;

import org.osgi.framework.ServiceReference;

public class Dashboard extends JFrame {

	private Map<ServiceReference, JPanel> probePanels = new HashMap<ServiceReference, JPanel>();
	private JPanel probesPanel = new JPanel();
	
	public Dashboard() {
		setLayout(new FlowLayout());
		probesPanel.setLayout(new GridLayout(0,1));
		add(new JLabel("Probes:"));
		add(probesPanel);
	}
	
	// Dependency manager lifecycle callback method
	void start() {
		pack();
		setVisible(true);
	}
	
	// Dependency manager lifecycle callback method
	void destroy() {
		dispose();
	}

	// Dependency manager injection callback method
	synchronized void addProbe(ServiceReference sr, Probe probe) {
		JPanel probePanel = new JPanel();
		probePanel.add(new JLabel(probe.getName()));
		probePanel.add(new JLabel(probe.getValue()));
		probePanels.put(sr, probePanel);
		probesPanel.add(probePanel);
		pack();
	}
	
	// Dependency manager injection callback method
	synchronized void removeProbe(ServiceReference sr, Probe probe) {
		probesPanel.remove(probePanels.remove(sr));
		pack();
	}
	
}
