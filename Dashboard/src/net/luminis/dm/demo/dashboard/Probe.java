package net.luminis.dm.demo.dashboard;

public interface Probe {

	/**
	 * Gets the name of the probe.
	 * @return
	 */
	String getName();
	
	/**
	 * Gets the value of the probe.
	 * @return
	 */
	String getValue();
	
}
